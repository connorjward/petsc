Installation
============
.. _petsc4py_install:


Using **pip**
-------------

You can use :program:`pip` to install :mod:`petsc4py` and its
dependencies (:mod:`mpi4py` is optional but highly recommended)::

  $ python -m pip install [--user] numpy mpi4py  (or pip install [--user] numpy mpi4py)
  $ python -m pip install [--user] petsc petsc4py (or pip install [--user] petsc petsc4py)

Note: One can build petsc4py via PETSc source build using the additional configure option --with-petsc4py=1



